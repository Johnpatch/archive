import React, { Component } from 'react';
import { Image, ScrollView, StyleSheet, Text, View, AsyncStorage, Dimensions } from 'react-native';
import { colors, images } from '../../themes/variables';
import Touchable from '../../Components/Touchable';
import { Container } from '../../Components/Container/Container';
import { Content } from '../../Components/Container/Content';
import StampModal from '../../Components/Stamp';
import Icon from 'react-native-vector-icons/Ionicons';
import { getCalendar, updateCalendar } from '../../api/calendar';
import { addEvent } from '../../api/event';
import { URLS } from '../../api/URLS';
import moment from "moment";
import LoadingModal from '../../Components/LoadingModal';
import ScrollPicker from 'react-native-wheely-simple-picker';
import { padLeft } from '../../utils/utils';
import * as Sentry from '@sentry/react-native';
import RNCalendarEvents from 'react-native-calendar-events';
import PushNotification from 'react-native-push-notification';
const screenWidth = Math.round(Dimensions.get('window').width);
class AlarmPage extends Component<any, any> {
    state = {
        showStamp: false,
        stamp: '',
        alarm: false,
        time: new Date(),
        id: '',
        calendarList: [] as any,
        isLoading: false,
        isLoadingEvent: false,
        hours: 0,
        eventId: ''
    };

    private unsubscribe = () => 0;

    async componentDidMount() {
        Sentry.addBreadcrumb({
            category: 'Navigation',
            message: 'Alarm page',
        });
        const { navigation, route } = this.props;
        this.unsubscribe = navigation.addListener('focus', async () => {
            await AsyncStorage.setItem("calendar_page", "1")
            await AsyncStorage.setItem("other_page", "1")
            //this.getCalendarList();
        });
        navigation.setOptions({
            title: "アラーム",
            headerStyle: {
                backgroundColor: colors.backColor,
                height: 50,
            },
            headerTitleStyle: {
                fontSize: 23,
                marginLeft: -20
            },
            headerTintColor: colors.fontColor,
            headerRight: () => (
                <Touchable onPress={this.addEventHandler}>
                    <Image source={images.Download} style={{ height: 30, width: 30, marginRight: 10 }} />
                </Touchable>
            ),
            headerLeft: () => (
                <Touchable onPress={() => navigation.navigate('Calender')}>
                    <Icon name="ios-arrow-back" size={45} color={colors.fontColor} style={{ marginLeft: 20 }} />
                </Touchable>
            )
        });
        this.getCalendarList();
    }

    componentWillUnmount(): void {
        this.unsubscribe();
    }

    getCalendarList = () => {
        this.setState({ isLoading: true });
        getCalendar()
            .then(response => {
                console.log(response.data)
                this.setState({ calendarList: response.data });
                this.setState({ isLoading: false });
            })
            .catch(err => {
                console.log(err.response)
                this.setState({ isLoading: false });
            })
    }

    stamp = (stamp: any, id: number) => {
        if (id) {
            const calendar = this.state.calendarList.find(p => p.id === id);
            calendar.stamp = stamp;
            calendar.dirty = true;
        }
        this.setState({ showStamp: !this.state.showStamp, stamp, id })
    }

    alarmChangeHandler = (id) => {
        const calendar = this.state.calendarList.find(p => p.id === id);
        let index = this.state.calendarList.indexOf(calendar)
        calendar.muted = !calendar.muted;
        calendar.dirty = true;
        this.state.calendarList[index] = calendar
        this.setState({calendarList: this.state.calendarList})
    }

    onTimeSelected = (id, date: number, key: 'hour' | 'minute') => {
        const calendar = this.state.calendarList.find(p => p.id === id);
        const time = calendar.time || moment(calendar.time).hours(0).minutes(0).seconds(0);
        if (key === 'hour') {
            time.hours(date)
        } else {
            time.minutes(date);
        }
        calendar.time = time;
        calendar[key] = date;
        calendar.dirty = true;
    }

    addEventHandler = () => {
        const { calendarList } = this.state;
        //this.setState({ isLoadingEvent: true });
        const updatedList = calendarList.filter(p => p.dirty);
        updatedList.forEach((e) => {
            updateCalendar({ ...e, dirty: undefined, stamp: undefined, time: undefined })
                .catch(r => console.log(r));
        });
        const eventToAdd = updatedList.filter(p => p.dirty && p.stamp && p.time ? p.time : this.state.time);
        
        eventToAdd.forEach(e => {
            let hour = 0;
            let minute = 0;
            if(e.hour)
                hour = e.hour
            if(e.minute)
                minute = e.minute
            let time = Date.now() + hour*3600000 + minute*60000;
            console.log(time)
            addEvent({ stamp: e.stamp, time: time, calendarId: e.id })
                .then(response => {
                    //console.log(response.data, "addEvent")
                    PushNotification.localNotificationSchedule({
                        //... You can use all the options from localNotifications
                        id: response.data.calendar.id.toString(),
                        message: "時間になりました。", 
                        date: new Date(time), 
                    });
                    this.saveEvents();
                    this.setState({ isLoadingEvent: false });
                })
                .catch(err => {
                    console.log(err.response)
                    this.setState({ isLoadingEvent: false });
                });
        })
    };

    saveEvents = async () => {
        let calendar = JSON.parse(await AsyncStorage.getItem('calendarId') as string);
        const title = `test-${calendar}`;
        const startDate = new Date().toISOString();
        const endDate = moment(new Date()).toISOString();
        const alarms = [{
            date: 1
        }]
        RNCalendarEvents.saveEvent(title, { startDate, endDate, alarms })
            .then(response => {
                console.log(response, 'save local events');
            })
            .catch(err => {
                console.log(err, 'save local event error')
            })
    }

    render() {
        const { calendarList, id, isLoading, isLoadingEvent, hours } = this.state;
        return (
            <Container style={{ flex: 1 }}>
                <LoadingModal isOpen={isLoading || isLoadingEvent} />
                <Content>
                    <StampModal isOpen={this.state.showStamp} close={(stamp: any) => this.stamp(stamp, parseInt(id))} />
                    <View style={styles.main}>
                        <Text style={styles.title}>選択したアイコンが設定時間内にカレンダーへ反映されなかった場合、参加ユーザーにアラームでお知らせします。</Text>
                        
                        {calendarList.map((a: any) => {
                            return (
                                <View key={a.id}>
                                    <View style={styles.header}>
                                        <Text style={styles.headerText}>{a.title}</Text>
                                        <Touchable style={{marginRight: 10}} onPress={() => this.alarmChangeHandler(a.id)}>
                                            <Image source={a.muted ? images.AlarmOn : images.AlarmOff} style={styles.alarmIcon} />
                                        </Touchable>
                                    </View>
                                    <Image source={a.background ? { uri: URLS.image + a.background } : images.Home} style={styles.image} />
                                    <View style={styles.addEventContainer}>
                                        <Touchable style={{ flexDirection: 'row', margin: 10 }}
                                            onPress={() => this.stamp('', a.id)}>
                                            <View style={styles.addContainer}>
                                                <Image
                                                    source={a.stamp ? { uri: URLS.stamp + a.stamp } : images.AddImage}
                                                    style={{ height: 50, width: 50 }}
                                                    resizeMode={"contain"} />
                                            </View>
                                        </Touchable>
                                        <View style={{ width: '60%', justifyContent: 'center', flexDirection: 'row', marginRight: 30, marginBottom: 20, height: 100}}>
                                            <ScrollPicker
                                                dataSource={[...Array(48)].map((p, i) => padLeft(i.toString(), 2))}
                                                selectedIndex={0}
                                                renderItem={(data, index, isSelected) => {
                                                }}
                                                onValueChange={(data, selectedIndex) => {
                                                    this.onTimeSelected(a.id, selectedIndex, 'hour')
                                                }}
                                                wrapperHeight={99}
                                                wrapperBackground="transparent"
                                                itemHeight={33}
                                                wrapperWidth={20}
                                                highlightBorderWidth={0}
                                                activeItemTextStyle={{color:'#747474', fontSize: 20}}
                                                itemTextStyle={{color: '#dadada', fontSize: 20}}
                                            />
                                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                                <Text>時間</Text>
                                            </View>
                                            <ScrollPicker
                                                dataSource={[...Array(60)].map((p, i) => padLeft(i.toString(), 2))}
                                                selectedIndex={0}
                                                renderItem={(data, index, isSelected) => {
                                                }}
                                                onValueChange={(data, selectedIndex) => {
                                                    this.onTimeSelected(a.id, selectedIndex, 'minute')
                                                }}
                                                wrapperHeight={99}
                                                wrapperBackground="transparent"
                                                itemHeight={33}
                                                wrapperWidth={20}
                                                highlightBorderWidth={0}
                                                activeItemTextStyle={{color:'#747474', fontSize: 20}}
                                                itemTextStyle={{color: '#dadada', fontSize: 20}}
                                            />
                                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                                <Text>分</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            );
                        })
                        }
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    main: {
        
    },
    container: {
        flexDirection: 'row',
        marginTop: 30
    },
    image: {
        height: screenWidth/2,
        width: screenWidth,
    },
    profile: {
        height: 100,
        width: 100,
        marginLeft: 20,
        borderRadius: 50
    },
    text: {
        fontSize: 18,
        borderBottomWidth: 1,
        marginRight: 20
    },
    alarmIcon: { height: 20, width: 32, marginRight: 5 },
    addEventContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        margin: 10,
    },
    header: {
        backgroundColor: colors.fontColor,
        padding: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    headerText: {
        fontSize: 16,
        color: colors.white,
        marginLeft: 10,
        fontFamily: "ms-pgothic",
    },
    title: {
        marginLeft: 10,
        paddingVertical: 15,
        fontFamily: "ms-pgothic",
        lineHeight: 25
    },
    addContainer: {
        backgroundColor: '#D7CFBF',
        height: 100,
        width: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 50,
        marginBottom: 20,
    }
})
export default AlarmPage;
