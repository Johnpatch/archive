import React, { Component } from 'react';
import { View, Text, StyleSheet, Switch, AsyncStorage } from 'react-native';
import Button from '../../Components/Buttons';
import { colors } from '../../themes/variables';
import * as Sentry from '@sentry/react-native';

class Other extends Component<any, any> {
    state = {
        notification: false,
        vibration: false
    }
    async componentDidMount(){
        await AsyncStorage.setItem("calendar_page", "1")
        Sentry.addBreadcrumb({
            category: 'Navigation',
            message: 'Other page',
        });
        const { navigation } = this.props;
        navigation.addListener('focus', async () => {
            await AsyncStorage.setItem("calendar_page", "1")
        });
    }
    notificationToggleSwitch = () => {
        this.setState({ notification: !this.state.notification });
    }

    vibrationToggleSwitch = () => {
        this.setState({ vibration: !this.state.vibration });
        // AsyncStorage.setItem('otherPage', !this.state.vibration);
    }

    helpScreenHandler = () => {
        this.props.navigation.navigate('Help')
    }

    termsOfUseScreenHandler = () => {
        this.props.navigation.navigate('TermsOfUse')
    }

    render() {
        const { notification, vibration } = this.state;
        return (
            <View style={styles.main}>
                <View style={styles.container}>
                    <View style={styles.innerContainer}>
                        <Text style={styles.text}>アプリの通知</Text>
                        <Switch trackColor={{ false: colors.switchBg, true: colors.fontColor }}
                            thumbColor={notification ? colors.backColor : colors.switch}
                            onValueChange={this.notificationToggleSwitch}
                            value={notification} />
                    </View>
                    <View style={[styles.innerContainer, { marginTop: 20 }]}>
                        <Text style={styles.text}>バイブレーション</Text>
                        <Switch trackColor={{ false: colors.switchBg, true: colors.fontColor }}
                            thumbColor={vibration ? colors.backColor : colors.switch}
                            onValueChange={this.vibrationToggleSwitch}
                            value={vibration} />
                    </View>
                </View>
                <View style={styles.button}>
                    <Button color={colors.backColor} title="ヘルプ" onPress={this.helpScreenHandler}></Button>
                    <Button color={colors.backColor} title="利用規約" onPress={this.termsOfUseScreenHandler}></Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    container: {
        flexDirection: 'column',
        marginTop: 30,
        marginLeft: 40
    },
    innerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 30
    },
    text: {
        fontSize: 16,
        color: colors.fontColor,
        fontFamily: "ms-pgothic",
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    }
})
export default Other;